# Geo - module for searching distance
module Geo
  # Api call for counting distance between two addresses
  def geo_count_distance(start_address, destination_address)
    # for test: use static to don't overload Geocoders
    return 10 if Rails.env.test? # if test env use defined distance
    return unless !start_address.blank? && !destination_address.blank?
    start = geo_find_address start_address
    destination = geo_find_address destination_address
    start.distance_to(destination, units: :kms)
  end

  # Api call for finding address
  def geo_find_address(address)
    Geokit::Geocoders::GoogleGeocoder.geocode address
  end
end
