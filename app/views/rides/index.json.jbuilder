json.array!(@rides) do |ride|
  json.extract! ride, :id, :corporation_id, :start_address, :destination_address, :date, :price
  json.url ride_url(ride, format: :json)
end
