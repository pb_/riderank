# Corporation model - taxi corporation names
class Corporation < ActiveRecord::Base
  scope :active, -> { where(status: true) }

  has_many :rides

  validates :name, presence: true
  validates :name, uniqueness: { case_sensitive: false }
  validates :name, length: { maximum: 256 }
end
