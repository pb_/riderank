# Ride model - list of rides
class Ride < ActiveRecord::Base
  include Geo

  default_scope { order(date: :asc) }
  scope :by_user_with_corporations, -> (user) { includes(:corporation).where(user: user) }
  scope :by_user, -> (user) { where(user: user) }
  scope :current_week, -> { where('date >= :date', date: Time.zone.now.at_beginning_of_week.strftime('%Y-%m-%d')) }
  scope :current_month, -> { where('date >= :date', date: Time.zone.now.at_beginning_of_month.strftime('%Y-%m-%d')) }
  scope :sum_price_and_distance, -> { select('sum(price) as price, sum(distance) as distance') }
  # MySQL: group_concat(corporations.name)
  # PostgreSQL: string_agg(corporations.name, ',')
  # Oracle: listagg(corporations.name, ',')
  scope :sum_distance_avg_distance_avg_price,
        lambda {
          select('sum(distance) as sum_distance,
                  avg(price) as avg_price, avg(distance) as avg_distance,
                  date,
                  GROUP_CONCAT(DISTINCT corporations.name) as corporations').joins(:corporation)
        }

  belongs_to :corporation

  validates :start_address, :destination_address, :user, :date, :price, :distance, :corporation, presence: true
  validates :start_address, :destination_address, :user, length: { maximum: 256 }
  validates :price, numericality: { greater_than_or_equal_to: 0.0 }
  validates :distance, numericality: { greater_than: 0.0, message: 'error. Check your address' }

  before_validation :count_distance

  delegate :name, to: :corporation, prefix: true

  # custom setter (changing comma to dot)
  def price=(val)
    val.sub!(',', '.') if val.is_a?(String)
    self['price'] = val
  end

  private

  # Counting distance from start to destination location
  def count_distance
    # check if similar ride exist in db, if yes take distance from it else count it
    ride = Ride.find_by_start_address_and_destination_address(start_address, destination_address)
    ride ? self.distance = ride.distance : self.distance = geo_count_distance(start_address, destination_address)
  end
end
