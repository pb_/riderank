# Application Helper
module ApplicationHelper
  # short version of number to currency, only for euro
  def number_to_eur(number)
    number_to_currency(number, unit: 'EUR', precision: 2, format: '%n %u')
  end
end
