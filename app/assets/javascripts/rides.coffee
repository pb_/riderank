# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
ready = ->
  Materialize.updateTextFields()
  Waves.displayEffect()
  $('.dropdown-button').dropdown()
  $(".button-collapse").sideNav()
  $('select').material_select ->
    $('.select-dropdown').removeClass('invalid')
  $('.datepicker').pickadate ->
    selectMonths: true, # Creates a dropdown to control month
    selectYears: 5 # Creates a dropdown of 5 years to control year

  #Check address when enter
  $('.form-class').on 'focusout', '#ride_start_address, #ride_destination_address', ->
    checkAddressFormat(this)

  # Before from submit
  $('.form-class').submit (e)->
    e.preventDefault()
    if (validateForm())
      address_ok = true
      if !checkAddressFormat('#ride_start_address')
        address_ok = false
      if !checkAddressFormat('#ride_destination_address')
        address_ok = false
      if address_ok
        $('.form-class')[0].submit()
      else
        showError('Please enter address in format "Street 9, City, Country"')
    else
      showError('Please fill in all fields')

$(document).ready(ready)
$(document).on('page:load', ready)

# Validate from
validateForm = ->
  isValid = true;
  $('.input-field > input').each ->
    if ( $(this).val() == '' )
      isValid = false
      $(this).addClass('invalid')
  if ( $('#ride_corporation_id').val() == '')
    isValid = false
    $('.select-dropdown').addClass('invalid')
  isValid

#Address format
checkAddressFormat = (id)->
  check = $(id).val().match(/,/g)
  if check and check.length == 2
    true
  else
    $(id).removeClass('valid')
    $(id).addClass('invalid')
    false

#Error
showError = (text)->
  Materialize.toast('<i class="material-icons">error_outline</i> &nbsp;&nbsp;&nbsp; ' + text, 4000, 'red darken-1')
