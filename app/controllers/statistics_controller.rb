# Stats Controller
class StatisticsController < ApplicationController
  def index
    # How many kilometers rode during current week and how much money spent on the rides
    @week_stats = Ride.sum_price_and_distance.current_week.by_user(current_user)
    # Summary of ride distances from current month, grouped by day.
    @month_stats = Ride.sum_distance_avg_distance_avg_price.current_month.by_user(current_user).group(:date)
  end
end
