# Application Controller
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :current_user

  def current_user
    @current_user ||= generate_token
  end

  private

  def generate_token
    cookies[:user] ? cookies.signed[:user] : cookies.permanent.signed[:user] = SecureRandom.uuid
  end
end
