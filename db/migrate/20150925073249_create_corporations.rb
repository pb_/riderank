# Creating table Corporations
class CreateCorporations < ActiveRecord::Migration
  def change
    create_table :corporations do |t|
      t.string :name
      t.boolean :status

      t.timestamps null: false
    end
  end
end
