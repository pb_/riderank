# Creating table Rides
class CreateRides < ActiveRecord::Migration
  def change
    create_table :rides do |t|
      t.references :corporation, index: true, foreign_key: true
      t.string :user
      t.string :start_address
      t.string :destination_address
      t.date :date, null: false, default: Time.zone.now
      t.decimal :price, precision: 6, scale: 2
      t.decimal :distance, precision: 6, scale: 2

      t.timestamps null: false
    end
  end
end
