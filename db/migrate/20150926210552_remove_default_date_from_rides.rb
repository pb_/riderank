# Removed default value from date field
class RemoveDefaultDateFromRides < ActiveRecord::Migration
  def change
    change_column_default(:rides, :date, nil)
  end
end
