# Add index to user
class AddIndexToRides < ActiveRecord::Migration
  def change
    add_index :rides, :user
  end
end
