# Add index to date and to start, destination location
class AddIndexesToRides < ActiveRecord::Migration
  def change
    add_index :rides, :date
    add_index :rides, [:start_address, :destination_address]
  end
end
