# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150927161408) do

  create_table "corporations", force: :cascade do |t|
    t.string   "name"
    t.boolean  "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rides", force: :cascade do |t|
    t.integer  "corporation_id"
    t.string   "user"
    t.string   "start_address"
    t.string   "destination_address"
    t.date     "date",                                        null: false
    t.decimal  "price",               precision: 6, scale: 2
    t.decimal  "distance",            precision: 6, scale: 2
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
  end

  add_index "rides", ["corporation_id"], name: "index_rides_on_corporation_id"
  add_index "rides", ["date"], name: "index_rides_on_date"
  add_index "rides", ["start_address", "destination_address"], name: "index_rides_on_start_address_and_destination_address"
  add_index "rides", ["user"], name: "index_rides_on_user"

end
