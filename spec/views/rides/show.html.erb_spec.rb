require 'rails_helper'

RSpec.describe 'rides/show', type: :view do
  before(:each) do
    @ride = assign(:ride, Fabricate(:ride, corporation: Fabricate(:corporation)))
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(/Start address/)
    expect(rendered).to match(/Destination address/)
    expect(rendered).to match(/Date/)
    expect(rendered).to match(/Price/)
  end
end
