require 'rails_helper'

RSpec.describe 'rides/index', type: :view do
  before(:each) do
    assign(:rides, [
      Fabricate(:ride, corporation: Fabricate(:corporation)),
      Fabricate(:ride, corporation: Fabricate(:sava))
    ])
  end

  it 'renders a list of rides' do
    render
    assert_select 'tr>td a', count: 6
  end
end
