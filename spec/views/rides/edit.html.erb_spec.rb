require 'rails_helper'

RSpec.describe 'rides/edit', type: :view do
  before(:each) do
    @ride = assign(:ride, Fabricate(:ride, corporation: Fabricate(:corporation)))
  end

  it 'renders the edit ride form' do
    render

    assert_select 'form[action=?][method=?]', ride_path(@ride), 'post' do
      assert_select 'input#ride_start_address[name=?]', 'ride[start_address]'

      assert_select 'input#ride_destination_address[name=?]', 'ride[destination_address]'

      assert_select 'input#ride_price[name=?]', 'ride[price]'

      assert_select 'input#ride_date[name=?]', 'ride[date]'
    end
  end
end
