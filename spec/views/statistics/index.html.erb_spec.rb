require 'rails_helper'

RSpec.describe 'statistics/index.html.erb', type: :view do
  before(:each) do
    @ride = Fabricate(:ride, date: 2.days.ago, corporation: Fabricate(:corporation))
    @ride2 = Fabricate(:ride, date: 3.days.ago, corporation: Fabricate(:corporation))

    @week_stats = assign(:week_stats, Ride.sum_price_and_distance.current_week)
    @month_stats = assign(:month_stats, Ride.sum_distance_avg_distance_avg_price.current_month.group(:date))
  end

  it 'renders a list of sections' do
    render
    expect(rendered).to match(/Statistics/)
    expect(rendered).to match(/Month/)
    expect(rendered).to match(/Week/)

    # Week stats
    expect(rendered).to have_selector('.section p', text: "Distance: #{@week_stats.first.distance}")
    expect(rendered).to have_selector('.section p', text: "Price: #{@week_stats.first.price}")

    # Month stats
    expect(rendered).to have_selector('table tbody tr:nth-of-type(1) td:nth-of-type(1)', text:
                       @month_stats.first.date.strftime('%d %B, %Y'))
    expect(rendered).to have_selector('table tbody tr:nth-of-type(1) td:nth-of-type(2)', text:
                       @month_stats.first.sum_distance)
    expect(rendered).to have_selector('table tbody tr:nth-of-type(1) td:nth-of-type(3)', text:
                       @month_stats.first.avg_distance)
    expect(rendered).to have_selector('table tbody tr:nth-of-type(1) td:nth-of-type(4)', text:
                       number_to_currency(@month_stats.first.avg_price, unit: 'EUR', precision: 2, format: '%n %u'))
    expect(rendered).to have_selector('table tbody tr:nth-of-type(1) td:nth-of-type(5)', text:
                       @month_stats.first.corporations.to_s.gsub(',', ', '))
  end
end
