require 'rails_helper'

feature 'Creating Rides' do
  scenario 'create a valid rides and check stats', js: true do
    visit root_path
    click_link 'add'

    fill_in 'Start address', with: 'Słomińskiego 7, Warszawa, Polska'
    fill_in 'Destination address', with: 'Marszłkowska 8, Warszawa, Polska'
    fill_in 'Price', with: 4.44
    find('.select-dropdown').click
    find('li', text: 'MPT').click
    click_button 'Submit'

    expect(page).to have_content('Start address: Słomińskiego 7, Warszawa, Polska')
    expect(page).to have_content('Destination address: Marszłkowska 8, Warszawa, Polska')
    expect(page).to have_content('Price: 4.44 EUR')

    click_link 'add'
    fill_in 'Start address', with: 'Marszłkowska 8, Warszawa, Polska'
    fill_in 'Destination address', with: 'Słomińskiego 7, Warszawa, Polska'
    fill_in 'Price', with: 4.04
    find('.select-dropdown').click
    find('li', text: 'Uber').click
    click_button 'Submit'

    click_link 'Statistics', match: :first
    expect(page).to have_content('Price: 8.48 EUR')
    expect(page).to have_content('4.24 EUR')
    expect(page).to have_content('MPT, Uber')
  end
end
