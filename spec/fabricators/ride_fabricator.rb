Fabricator(:ride) do
  corporation
  user 'bd306e34-b294-433a-9a5c-5d1dbb014653'
  start_address 'Marszałkowska 28, Warszawa, Polska'
  destination_address 'Puławska 8, Warszawa, Polska'
  date { Faker::Date.backward(10) }
  price { Faker::Commerce.price }
  distance { Faker::Number.number(2) }
end
