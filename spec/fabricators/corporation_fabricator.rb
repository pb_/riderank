Fabricator(:corporation) do
  name { Faker::Name.name }
  status true
end

Fabricator(:sava, from: :corporation) do
  name 'Sava'
end
