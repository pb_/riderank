require 'rails_helper'

RSpec.describe Corporation, type: :model do
  it { should validate_uniqueness_of(:name).case_insensitive }
  it { should have_many(:rides) }

  it 'is a valid with name and status' do
    expect(Fabricate.build(:corporation)).to be_valid
  end

  it 'is invalid without a name' do
    expect(Fabricate.build(:corporation, name: nil)).to_not be_valid
  end

  it 'is invalid with a name length longer than 256' do
    expect(Fabricate.build(:corporation, name: 'abc' * 1000)).to_not be_valid
  end

  it 'is valid without a status' do
    expect(Fabricate.build(:corporation, status: nil)).to be_valid
  end

  context 'scopes' do
    before(:all) do
      @active = Fabricate(:corporation)
      @inactive = Fabricate(:corporation, status: false)
    end

    it 'should only return active corporation' do
      expect(Corporation.active).to include(@active)
      expect(Corporation.active).not_to include(@inactive)
    end
  end
end
