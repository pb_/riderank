require 'rails_helper'

RSpec.describe Ride, type: :model do
  it { should belong_to(:corporation) }
  it { should validate_presence_of(:start_address) }
  it { should validate_presence_of(:destination_address) }
  it { should validate_presence_of(:price) }
  it { should validate_presence_of(:date) }
  it { should validate_presence_of(:user) }
  it { should validate_numericality_of(:price).is_greater_than_or_equal_to(0.0) }

  context 'scopes' do
    before(:all) do
      @ride = Fabricate(:ride, date: Time.zone.now.at_beginning_of_month, user: 123, corporation: Fabricate(:corporation))
      @ride2 = Fabricate(:ride, date: Time.zone.now.at_beginning_of_month, corporation: Fabricate(:corporation))
      @ride3 = Fabricate(:ride, date: 40.days.ago, corporation: Fabricate(:corporation))
    end

    it 'should only return current week rides' do
      expect(Ride.current_week).to include(@ride)
      expect(Ride.current_week).not_to include(@ride3)
    end

    it 'should only return current month rides' do
      expect(Ride.current_month).to include(@ride, @ride2)
      expect(Ride.current_month).not_to include(@ride3)
    end

    it 'should only return current user rides' do
      expect(Ride.by_user(123)).to include(@ride)
      expect(Ride.by_user(123)).not_to include(@ride2, @ride3)
    end

    before do
      @user666_ride1 = Fabricate(:ride, user: 100, corporation: Fabricate(:corporation))
      @user666_ride2 = Fabricate(:ride, user: 100, corporation: Fabricate(:corporation))
    end
    it 'should return sum price and distance' do
      sum_price = @user666_ride1.price + @user666_ride2.price
      sum_distance = @user666_ride1.distance + @user666_ride2.distance
      expect(Ride.sum_price_and_distance.by_user(100).first.price).to eq(sum_price)
      expect(Ride.sum_price_and_distance.by_user(100).first.distance).to eq(sum_distance)
    end

    it 'should return sum distance, avg distance and avg price' do
      sum_distance = @user666_ride1.distance + @user666_ride2.distance
      avg_distance = (@user666_ride1.distance + @user666_ride2.distance) / 2
      avg_price = (@user666_ride1.price + @user666_ride2.price) / 2
      expect(Ride.sum_distance_avg_distance_avg_price.by_user(100).first.sum_distance).to eq(sum_distance)
      expect(Ride.sum_distance_avg_distance_avg_price.by_user(100).first.avg_distance).to eq(avg_distance)
      expect(Ride.sum_distance_avg_distance_avg_price.by_user(100).first.avg_price).to eq(avg_price)
    end
  end
end
