require 'rails_helper'

RSpec.describe 'Rides', type: :request do
  describe 'GET /rides' do
    it 'root path' do
      get root_path
      expect(response).to have_http_status(200)
    end
    it 'rides path' do
      get rides_path
      expect(response).to have_http_status(200)
    end
    it 'new rides path' do
      get new_ride_path
      expect(response).to have_http_status(200)
    end
    it 'stats path' do
      get statistics_index_path
      expect(response).to have_http_status(200)
    end
  end
end
